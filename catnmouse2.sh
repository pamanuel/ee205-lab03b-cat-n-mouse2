#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo OK cat, I\’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess


while true;
do 
   echo Enter a number:
   read aGuess
   echo The number was $aGuess

   if [ $aGuess -le 1 ]
   then
	 echo "You must enter a number that’s >= 1"
	 continue;
   fi 

   if [ $aGuess -ge $THE_MAX_VALUE ]
   then
	 echo "You must enter a number that’s <=" $THE_MAX_VALUE
	 continue;
   fi

   if [ $aGuess -gt $THE_NUMBER_IM_THINKING_OF ]
   then
   	 echo No cat... the number I\’m thinking of is smaller than $aGuess
   fi

   if [ $aGuess -lt $THE_NUMBER_IM_THINKING_OF ]
   then
	   echo No cat... the number I\’m thinking of is larger than $aGuess
   fi

   if [ $aGuess == $THE_NUMBER_IM_THINKING_OF ]
   then
         echo You got me
	 echo " /\_/\ " 
         echo "( o.o )" 
     	 echo " > ^ < "
       break	 
   fi
done

